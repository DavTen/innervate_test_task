import moment from 'moment';
import crypto from 'crypto';
import {oncePerServices, missingService} from '../../common/services/index';

function apolloToRelayResolverAdapter(oldResolver) {
  return function (obj, args, context) {
    return oldResolver(args, context.request);
  }
}

export default oncePerServices(function (services) {
    
    const {
        postgres = missingService('postgres')
    } = services;
  
  function testQuery(builderContext) {
    return async function(obj, args, context) {
      return [
          {str: "A", int: 1, obj: {a: "A1", b: "B1"}},
          {str: "B", int: 2, obj: {a: "A2", b: "B2"}},
          {str: "C", int: 3, obj: {a: "A3", b: "B3"}},
      ];
    }
  }

  function getUsers(builderContext) {
      return async function(obj, args, context) {
          let statement = `select * from users`;
          let conditions = Object.keys(args).map(field => {
              switch (typeof(args[field])) {
                  case "number": return `${field}=${args[field]}`; break;
                  case "string": return `${field} like '%${args[field]}%'`; break;
                  case "boolean": if (args[field]) {
                      return field;
                  }  else {
                      return `(not ${field} or ${field} is null)`;
                  }
                  break;
              }
          });
          if (conditions.length) statement += ` WHERE ${conditions.join(" AND ")}`;
          return postgres.exec({
              statement: statement,
              params: []
          }).then((res) => {
              return res.rows.map(row => {
                  return {
                      id: row.user_id,
                      login: row.login,
                      name: row.name,
                      email: row.email,
                      manager: row.manager || false,
                      blocked: row.blocked || false,
                      birthday: row.data ? row.data.birthday || '' : ''
                  }
              });
          });
      }
  }

  function auth(builderContext) {
      return async function(obj, args, context) {
          if (!args.login || !args.password) {
              throw new Error("Arguments are not passed: login or password");
          }
          let login = args.login;
          let hash = crypto.createHash("md5").update(args.password).digest("hex");
          return postgres.exec({
              statement: `select user_id from users where login='${login}' and password_hash='${hash}'`
          }).then(res => {
             return [{success: res.rows.length > 0}]
          });
      }
  }

  return {
    testQuery,
    getUsers,
    auth
  }
});
